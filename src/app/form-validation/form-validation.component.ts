//@ts-nocheck
import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { FormsModule }   from '@angular/forms';

@Component({
  selector: 'app-form-validation',
  templateUrl: './form-validation.component.html',
  styleUrls: ['./form-validation.component.css']
})
export class FormValidationComponent implements OnInit {
  nameMsg: String;
  dobMsg: String;
  ageMsg: String;
  phoneNumberMsg: String;
  emailMsg: String;
  feedbackMsg: String;

  constructor() { }

  ngOnInit(): void {
  }  
  onSubmit(formValue: any) {
    console.log(formValue);    
    this.nameMsg = '';
    this.ageMsg = ''; 

    if (!formValue.name || formValue.name.length < 3) {
      this.nameMsg = 'Name is required and must be at least 3 characters long!';
    }
    if (formValue.name.match(/^[a-zA-Z0-9]+$/) == null) {
      this.nameMsg = 'Name must be alphanumeric!';
    }
    if (!formValue.age) {
      this.ageMsg = 'Age is required!';
    }
    if (formValue.age<=0 || formValue.age>=200) {
      this.ageMsg = 'Age must >0 and <200!';
    }
    // console.log(formValue.name);
    // console.log(formValue.age);
    // if (!formValue.name || !formValue.age || formValue.age < 0 || formValue.age > 200)
    // {
    //   //alert("Input Error!");
    //   this.error.msg = 'Input Error!';
    // } else {
    //   this.error.msg = '';
    // }
  }
}